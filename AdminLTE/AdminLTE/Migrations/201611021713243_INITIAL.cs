namespace AdminLTE.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class INITIAL : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "adm.account",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        bankId = c.Int(nullable: false),
                        currencyId = c.Int(nullable: false),
                        typeAccount = c.String(),
                        numberAccount = c.String(nullable: false, maxLength: 20),
                        balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.bank", t => t.bankId, cascadeDelete: true)
                .ForeignKey("adm.currency", t => t.currencyId, cascadeDelete: true)
                .Index(t => new { t.bankId, t.numberAccount }, unique: true, name: "INDEX_ADM_ACCOUNT")
                .Index(t => t.currencyId)
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_ACCOUNT_UNIQUEFIELD");
            
            CreateTable(
                "adm.bank",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        acronym = c.String(maxLength: 128),
                        name = c.String(nullable: false, maxLength: 128),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.name, unique: true, name: "INDEX_ADM_BANK_NAME")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_BANK_UNIQUEFIELD");
            
            CreateTable(
                "adm.currency",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        description = c.String(maxLength: 128),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.name, unique: true, name: "INDEX_ADM_CURRENCY_NAME")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_CURRENCY_UNIQUEFIELD");
            
            CreateTable(
                "adm.checkdetail",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        checkId = c.Int(nullable: false),
                        name = c.String(nullable: false, maxLength: 128),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.check", t => t.checkId, cascadeDelete: true)
                .Index(t => new { t.checkId, t.name }, unique: true, name: "INDEX_ADM_CHECKDETAIL")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_CHECKDETAIL_UNIQUEFIELD");
            
            CreateTable(
                "adm.check",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        checkNumber = c.String(nullable: false, maxLength: 20),
                        date = c.DateTime(nullable: false),
                        description = c.String(maxLength: 128),
                        clientId = c.Int(nullable: false),
                        currencyId = c.Int(nullable: false),
                        checkStateId = c.Int(nullable: false),
                        exchangeRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.checkstate", t => t.checkStateId, cascadeDelete: true)
                .ForeignKey("adm.client", t => t.clientId, cascadeDelete: true)
                .ForeignKey("adm.currency", t => t.currencyId, cascadeDelete: true)
                .Index(t => t.checkNumber, unique: true, name: "INDEX_ADM_CHECK_CHECKNUMBER")
                .Index(t => t.clientId)
                .Index(t => t.currencyId)
                .Index(t => t.checkStateId)
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_CHECK_UNIQUEFIELD");
            
            CreateTable(
                "adm.checkstate",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        description = c.String(maxLength: 128),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.name, unique: true, name: "INDEX_ADM_CHECKSTATE_NAME")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_CHECKSTATE_UNIQUEFIELD");
            
            CreateTable(
                "adm.client",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        typeIdentificationId = c.Int(nullable: false),
                        identification = c.String(maxLength: 20),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.typeidentification", t => t.typeIdentificationId, cascadeDelete: true)
                .Index(t => t.name, unique: true, name: "INDEX_ADM_CLIENT_NAME")
                .Index(t => t.typeIdentificationId)
                .Index(t => t.identification, unique: true, name: "INDEX_ADM_CLIENT_IDENTIFICATION")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_CLIENT_UNIQUEFIELD");
            
            CreateTable(
                "adm.typeidentification",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        description = c.String(maxLength: 128),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.name, unique: true, name: "INDEX_ADM_TYPEIDENTIFICATION_NAME")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_TYPEIDENTIFICATION_UNIQUEFIELD");
            
            CreateTable(
                "adm.detailofsignaturesbycheck",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        signatureId = c.Int(nullable: false),
                        checkId = c.Int(nullable: false),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.check", t => t.checkId, cascadeDelete: true)
                .ForeignKey("adm.signature", t => t.signatureId, cascadeDelete: true)
                .Index(t => new { t.signatureId, t.checkId }, unique: true, name: "INDEX_ADM_DETAILOFSIGNATURESBYCHECK")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_DETAILOFSIGNATURESBYCHECK_UNIQUEFIELD");
            
            CreateTable(
                "adm.signature",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        type = c.String(nullable: false, maxLength: 3),
                        description = c.String(maxLength: 128),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.name, unique: true, name: "INDEX_ADM_SIGNATURE_NAME")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_SIGNATURE_UNIQUEFIELD");
            
            CreateTable(
                "adm.detailofsignaturesbyaccount",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        signatureId = c.Int(nullable: false),
                        accountId = c.Int(nullable: false),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.account", t => t.accountId, cascadeDelete: true)
                .ForeignKey("adm.signature", t => t.signatureId, cascadeDelete: true)
                .Index(t => new { t.signatureId, t.accountId }, unique: true, name: "INDEX_ADM_DETAILOFSIGNATURESBYACCOUNT")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_DETAILOFSIGNATURESBYACCOUNT_UNIQUEFIELD");
            
            CreateTable(
                "adm.exchangerate",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        currencyId = c.Int(nullable: false),
                        date = c.DateTime(nullable: false),
                        value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        uniquefield = c.String(nullable: false, maxLength: 128),
                        active = c.Boolean(nullable: false),
                        fieldControl = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        dateCreation = c.DateTime(nullable: false),
                        dateModification = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("adm.currency", t => t.currencyId, cascadeDelete: true)
                .Index(t => t.currencyId)
                .Index(t => t.date, unique: true, name: "INDEX_ADM_EXCHANGERATE_DATE")
                .Index(t => t.uniquefield, unique: true, name: "INDEX_ADM_EXCHANGERATE_UNIQUEFIELD");
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("adm.exchangerate", "currencyId", "adm.currency");
            DropForeignKey("adm.detailofsignaturesbyaccount", "signatureId", "adm.signature");
            DropForeignKey("adm.detailofsignaturesbyaccount", "accountId", "adm.account");
            DropForeignKey("adm.detailofsignaturesbycheck", "signatureId", "adm.signature");
            DropForeignKey("adm.detailofsignaturesbycheck", "checkId", "adm.check");
            DropForeignKey("adm.checkdetail", "checkId", "adm.check");
            DropForeignKey("adm.check", "currencyId", "adm.currency");
            DropForeignKey("adm.check", "clientId", "adm.client");
            DropForeignKey("adm.client", "typeIdentificationId", "adm.typeidentification");
            DropForeignKey("adm.check", "checkStateId", "adm.checkstate");
            DropForeignKey("adm.account", "currencyId", "adm.currency");
            DropForeignKey("adm.account", "bankId", "adm.bank");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("adm.exchangerate", "INDEX_ADM_EXCHANGERATE_UNIQUEFIELD");
            DropIndex("adm.exchangerate", "INDEX_ADM_EXCHANGERATE_DATE");
            DropIndex("adm.exchangerate", new[] { "currencyId" });
            DropIndex("adm.detailofsignaturesbyaccount", "INDEX_ADM_DETAILOFSIGNATURESBYACCOUNT_UNIQUEFIELD");
            DropIndex("adm.detailofsignaturesbyaccount", "INDEX_ADM_DETAILOFSIGNATURESBYACCOUNT");
            DropIndex("adm.signature", "INDEX_ADM_SIGNATURE_UNIQUEFIELD");
            DropIndex("adm.signature", "INDEX_ADM_SIGNATURE_NAME");
            DropIndex("adm.detailofsignaturesbycheck", "INDEX_ADM_DETAILOFSIGNATURESBYCHECK_UNIQUEFIELD");
            DropIndex("adm.detailofsignaturesbycheck", "INDEX_ADM_DETAILOFSIGNATURESBYCHECK");
            DropIndex("adm.typeidentification", "INDEX_ADM_TYPEIDENTIFICATION_UNIQUEFIELD");
            DropIndex("adm.typeidentification", "INDEX_ADM_TYPEIDENTIFICATION_NAME");
            DropIndex("adm.client", "INDEX_ADM_CLIENT_UNIQUEFIELD");
            DropIndex("adm.client", "INDEX_ADM_CLIENT_IDENTIFICATION");
            DropIndex("adm.client", new[] { "typeIdentificationId" });
            DropIndex("adm.client", "INDEX_ADM_CLIENT_NAME");
            DropIndex("adm.checkstate", "INDEX_ADM_CHECKSTATE_UNIQUEFIELD");
            DropIndex("adm.checkstate", "INDEX_ADM_CHECKSTATE_NAME");
            DropIndex("adm.check", "INDEX_ADM_CHECK_UNIQUEFIELD");
            DropIndex("adm.check", new[] { "checkStateId" });
            DropIndex("adm.check", new[] { "currencyId" });
            DropIndex("adm.check", new[] { "clientId" });
            DropIndex("adm.check", "INDEX_ADM_CHECK_CHECKNUMBER");
            DropIndex("adm.checkdetail", "INDEX_ADM_CHECKDETAIL_UNIQUEFIELD");
            DropIndex("adm.checkdetail", "INDEX_ADM_CHECKDETAIL");
            DropIndex("adm.currency", "INDEX_ADM_CURRENCY_UNIQUEFIELD");
            DropIndex("adm.currency", "INDEX_ADM_CURRENCY_NAME");
            DropIndex("adm.bank", "INDEX_ADM_BANK_UNIQUEFIELD");
            DropIndex("adm.bank", "INDEX_ADM_BANK_NAME");
            DropIndex("adm.account", "INDEX_ADM_ACCOUNT_UNIQUEFIELD");
            DropIndex("adm.account", new[] { "currencyId" });
            DropIndex("adm.account", "INDEX_ADM_ACCOUNT");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("adm.exchangerate");
            DropTable("adm.detailofsignaturesbyaccount");
            DropTable("adm.signature");
            DropTable("adm.detailofsignaturesbycheck");
            DropTable("adm.typeidentification");
            DropTable("adm.client");
            DropTable("adm.checkstate");
            DropTable("adm.check");
            DropTable("adm.checkdetail");
            DropTable("adm.currency");
            DropTable("adm.bank");
            DropTable("adm.account");
        }
    }
}
