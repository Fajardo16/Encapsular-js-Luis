﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminLTE.Models;
using AdminLTE.ReportsDesign;

namespace AdminLTE.Controllers
{
    public class checksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: checks
        [AllowAnonymous]
        public ActionResult Index()
        {
            //Log4NetHelper.Log("Prueba_log!", LogLevel.INFO, "TEST", 0, "Tester", null);
            var checks = db.checks.Include(c => c.checkStates).Include(c => c.clients).Include(c => c.currencys);
            return View(checks.ToList());
            //return View();
        }

        public JsonResult getCheck()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checks = db.checks.Include(c => c.checkStates).Include(c => c.clients).Include(c => c.currencys);
            List<check> checksList = checks.ToList();
            return Json(checksList, JsonRequestBehavior.AllowGet);
        }

        // GET: checks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            check check = db.checks.Find(id);
            if (check == null)
            {
                return HttpNotFound();
            }
            return View(check);
        }

        // GET: checks/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            //db.Configuration.ProxyCreationEnabled = false;
            //ViewBag.checkStateId = new SelectList(db.checkStates, "id", "name");
            //ViewBag.signatures = new SelectList(db.signatures, "id", "name");
            checkState checkStateIdCreated = db.checkStates.FirstOrDefault(cs => cs.name == "Creado");
            ViewBag.checkStateId = checkStateIdCreated.id;
            ViewBag.clientId = new SelectList(db.clients, "id", "name");
            ViewBag.currencyId = new SelectList(db.currencys, "id", "name");
            ViewBag.date = DateTime.Now;

            exchangeRate exchangeRateValue = db.exchangeRates.FirstOrDefault(er => er.date.Day == DateTime.Now.Day
                && er.date.Month == DateTime.Now.Month
                && er.date.Year == DateTime.Now.Year);
            ViewBag.exchangeRate = exchangeRateValue.value;

            return View();
        }

        // POST: checks/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,checkNumber,date,description,clientId,currencyId,checkStateId,exchangeRate,amount,uniquefield,active,fieldControl,dateCreation,dateModification")] check check)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.checks.Add(check);
                    db.SaveChanges();
                    Log4NetHelper.Log(String.Format("cheque con ID: {0}, Creado!", check.id), LogLevel.INFO, "Check", check.id, User.Identity.Name, null);
                    //return RedirectToAction("Index");
                    return RedirectToAction("Edit", new { controller = "checks", action = "Edit", uniquefield = check.uniquefield });
                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    Log4NetHelper.Log(String.Format("Error al crear un cheque"), LogLevel.ERROR, "Check", 0, User.Identity.Name, ex);
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:
                                TempData["MessageToClient"] = String.Format("¡No se puede duplicar el numero de cheque!.");
                                //ViewBag.messageToClient = String.Format("¡Esta firma ya esta registrada en el cheque!.");
                                break;
                            default:
                                throw;
                        }
                }
                
            }

            ViewBag.checkStateId = new SelectList(db.checkStates, "id", "name", check.checkStateId);
            ViewBag.clientId = new SelectList(db.clients, "id", "name", check.clientId);
            ViewBag.currencyId = new SelectList(db.currencys, "id", "name", check.currencyId);
            return View(check);
        }

        // GET: checks/Edit/5
        public ActionResult Edit(string uniquefield)
        {
            if (uniquefield == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            //check check = db.checks.Find(id);
            check check = db.checks.FirstOrDefault(c => c.uniquefield == uniquefield);
            if (check == null)
            {
                return HttpNotFound();
            }

            ViewBag.checkStateId = new SelectList(db.checkStates, "id", "name", check.checkStateId);
            ViewBag.clientId = new SelectList(db.clients, "id", "name", check.clientId);
            ViewBag.currencyId = new SelectList(db.currencys, "id", "name", check.currencyId);
            //ViewBag.currentSearch = search;
            //ViewBag.msgError = msgError;
            ViewBag.checkId = check.id;
            //ViewBag.page = page;
            //ViewBag.sort = sort;
            return View(check);
        }

        // POST: checks/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,checkNumber,date,description,clientId,currencyId,checkStateId,exchangeRate,amount,uniquefield,active,fieldControl,dateCreation,dateModification")] check check)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();
                    Log4NetHelper.Log(String.Format("cheque con ID: {0}, Modificado!", check.id), LogLevel.INFO, "Check", check.id, User.Identity.Name, null);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    Log4NetHelper.Log(String.Format("Error al modificar el cheque con ID: {0}",check.id), LogLevel.ERROR, "Check", check.id, User.Identity.Name, ex);
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:
                                TempData["msgErrorSignature"] = String.Format("¡El numero de cheque ya existe!");
                                return new ContentResult();
                                break;
                            default:
                                TempData["msgErrorSignature"] = String.Format(e.Message);
                                return new ContentResult();
                        }
                }
                
            }
            ViewBag.checkStateId = new SelectList(db.checkStates, "id", "name", check.checkStateId);
            ViewBag.clientId = new SelectList(db.clients, "id", "name", check.clientId);
            ViewBag.currencyId = new SelectList(db.currencys, "id", "name", check.currencyId);
            return View(check);
        }

        // GET: checks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            check check = db.checks.Find(id);
            if (check == null)
            {
                return HttpNotFound();
            }
            return View(check);
        }

        // POST: checks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            check check = db.checks.Find(id);
            db.checks.Remove(check);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl)
        {
            string strUrl = "";
            if (Request.IsSecureConnection)
                strUrl = string.Format("https://{0}{1}{2}", Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port.ToString(), "/");
            else
                strUrl = string.Format("http://{0}{1}{2}", Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port.ToString(), "/");
            strUrl = strUrl + relativeUrl;
            return strUrl;
        }

        public ActionResult DownloadPDF(int checkId, string search)
        {
            generateMovementsReport(Server.MapPath("~/Reports/"), ConvertRelativeUrlToAbsoluteUrl("Reports/"), "PDF", checkId, search);

            return File("~/Reports/checkMovements.pdf", "application/pdf", "checkMovements.pdf");
        }

        public ActionResult DownloadXLS(int checkId, string search)
        {
            generateMovementsReport(Server.MapPath("~/Reports/"), ConvertRelativeUrlToAbsoluteUrl("Reports/"), "XLS", checkId, search);

            return File("~/Reports/checkMovements.xls", "application/vnd.ms-excel", "checkMovements.xls");
        }

        public string generateMovementsReport(string urlAbs, string urlRel, string report, int checkId, string search)
        {


            urlAbs = (urlAbs == null ? "" : urlAbs);
            urlRel = (urlRel);


            string urlDir = "";

            var mc = new checkViews().movementByCheck(checkId);

            if (!string.IsNullOrEmpty(search)) mc = mc.Where(cd => cd.movement.Contains(search));

            List<checkViews> listMovementByCheck = mc.ToList();

            System.Data.DataTable tTemp = new System.Data.DataTable();
            tTemp.Columns.Add("checkState", System.Type.GetType("System.String"));
            tTemp.Columns.Add("checkNumber", System.Type.GetType("System.String"));
            tTemp.Columns.Add("date", System.Type.GetType("System.DateTime"));
            tTemp.Columns.Add("client", System.Type.GetType("System.String"));
            tTemp.Columns.Add("description", System.Type.GetType("System.String"));
            tTemp.Columns.Add("currency", System.Type.GetType("System.String"));
            tTemp.Columns.Add("exchangeRate", System.Type.GetType("System.Decimal"));
            tTemp.Columns.Add("totalAmount", System.Type.GetType("System.Decimal"));
            tTemp.Columns.Add("movement", System.Type.GetType("System.String"));
            tTemp.Columns.Add("movementAmount", System.Type.GetType("System.Decimal"));


            foreach (checkViews item in listMovementByCheck)
            {

                System.Data.DataRow r = tTemp.NewRow();

                r["checkState"] = item.checkState;
                r["checkNumber"] = item.checkNumber;
                r["date"] = item.date;
                r["client"] = item.client;
                r["description"] = item.description;
                r["currency"] = item.currency;
                r["exchangeRate"] = item.exchangeRate;
                r["totalAmount"] = item.totalAmount;
                r["movement"] = item.movement;
                r["movementAmount"] = item.movementAmount;


                tTemp.Rows.Add(r);
            }

            string fileName = "checkMovements.txt";
            string sourcePath = @urlAbs;


            if (report == "PDF")
            {
                //Permiso Imp. Industrial
                if (!System.IO.File.Exists(urlAbs + "checkMovements" + ".pdf"))
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument rpt = new checkMovementsReport();
                    rpt.SetDataSource(tTemp);
                    //rpt.SetParameterValue("TitleReport", "Reporte de Mercado");
                    //rpt.SetParameterValue("startDate", dateadded);
                    //rpt.SetParameterValue("endDate", datemodified);
                    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat,
                        urlAbs + "checkMovements" + ".pdf");
                    urlDir = urlRel + "checkMovements" + ".pdf";

                    //this.DownloadPDF();
                    rpt.Close();
                    rpt.Dispose();
                }
                else
                {
                    try
                    {
                        System.IO.File.Delete(@urlAbs + "checkMovements" + ".pdf");

                        CrystalDecisions.CrystalReports.Engine.ReportDocument rpt = new checkMovementsReport();
                        rpt.SetDataSource(tTemp);
                        //rpt.SetParameterValue("TitleReport", "Reporte de Mercado");
                        //rpt.SetParameterValue("startDate", dateadded);
                        //rpt.SetParameterValue("endDate", datemodified);
                        rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat,
                            urlAbs + "checkMovements" + ".pdf");
                        urlDir = urlRel + "checkMovements" + ".pdf";

                        //DownloadPDF();
                        rpt.Close();
                        rpt.Dispose();
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                        return "";
                    }


                    //urlDir = urlRel + "seedAgro_Ventas" + ".pdf";
                }
            }
            else
            {

                //Permiso Imp. Industrial
                if (!System.IO.File.Exists(urlAbs + "checkMovements" + ".xls"))
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument rpt = new checkMovementsReport();
                    rpt.SetDataSource(tTemp);
                    //rpt.SetParameterValue("TitleReport", "Reporte de Mercado");
                    //rpt.SetParameterValue("startDate", dateadded);
                    //rpt.SetParameterValue("endDate", datemodified);
                    rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel,
                        urlAbs + "checkMovements" + ".xls");
                    urlDir = urlRel + "checkMovements" + ".xls";

                    //this.DownloadPDF();
                    rpt.Close();
                    rpt.Dispose();
                }
                else
                {
                    try
                    {
                        System.IO.File.Delete(@urlAbs + "checkMovements" + ".xls");

                        CrystalDecisions.CrystalReports.Engine.ReportDocument rpt = new checkMovementsReport();
                        rpt.SetDataSource(tTemp);
                        //rpt.SetParameterValue("TitleReport", "Reporte de Mercado");
                        //rpt.SetParameterValue("startDate", dateadded);
                        //rpt.SetParameterValue("endDate", datemodified);
                        rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel,
                            urlAbs + "checkMovements" + ".xls");
                        urlDir = urlRel + "checkMovements" + ".xls";

                        //DownloadPDF();
                        rpt.Close();
                        rpt.Dispose();
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                        return "";
                    }


                    //urlDir = urlRel + "seedAgro_Ventas" + ".pdf";
                }
            }


            return urlDir;

        }

        public ActionResult Calendar()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
