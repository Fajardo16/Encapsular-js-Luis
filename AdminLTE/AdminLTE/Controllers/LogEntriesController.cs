﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminLTE.Models;
using PagedList;

namespace AdminLTE.Controllers
{
    [Authorize(Roles = "Admin")]
    public class LogEntriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: LogEntries
        public ActionResult Index(String sort, string search, int? page)
        {
            ViewBag.LogLevelSort = String.IsNullOrEmpty(sort) ? "LogLevel_desc" : string.Empty;
            ViewBag.LogDateSort = sort == "LogDate" ? "LogDate_desc" : "LogDate";
            ViewBag.EntityFormalNamePluralSort = sort == "EntityFormalNamePlural" ? "EntityFormalNamePlural_desc" : "EntityFormalNamePlural";
            ViewBag.UserNameSort = sort == "UserName" ? "UserName_desc" : "UserName";
            ViewBag.MessageSort = sort == "Message" ? "Message_desc" : "Message";
            ViewBag.ExceptionSort = sort == "Exception" ? "Exception_desc" : "Exception";

            ViewBag.CurrentSort = sort;
            ViewBag.CurrentSearch = search;

            IQueryable<LogEntry> logEntries = db.LogEntries.OrderByDescending(le => le.LogEntryID);

            if (!string.IsNullOrEmpty(search)) logEntries = logEntries.Where(ii => ii.LogLevel.Contains(search) || ii.EntityFormalNamePlural.Contains(search) || ii.UserName.Contains(search) || ii.Message.Contains(search) || ii.Exception.Contains(search));

            switch (sort)
            {
                case "LogLevel_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.LogLevel);
                    break;

                case "LogDate":
                    logEntries = logEntries.OrderBy(ii => ii.LogDate);
                    break;

                case "LogDate_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.LogDate);
                    break;

                case "EntityFormalNamePlural":
                    logEntries = logEntries.OrderBy(ii => ii.EntityFormalNamePlural);
                    break;

                case "EntityFormalNamePlural_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.EntityFormalNamePlural);
                    break;

                case "UserName":
                    logEntries = logEntries.OrderBy(ii => ii.UserName);
                    break;

                case "UserName_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.UserName);
                    break;

                case "Message":
                    logEntries = logEntries.OrderBy(ii => ii.Message);
                    break;

                case "Message_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.Message);
                    break;

                case "Exception":
                    logEntries = logEntries.OrderBy(ii => ii.Exception);
                    break;

                case "Exception_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.Exception);
                    break;

                default:
                    logEntries = logEntries.OrderBy(ii => ii.LogLevel);
                    break;

            }



            int pageSize = 5;
            int pageNumber = page ?? 1;

            //return View(logEntries.ToPagedList(pageNumber, pageSize));

            return View(logEntries.ToPagedList(pageNumber, pageSize));
        }

        //Index para usuario
        public ActionResult IndexUser(String sort, string search, int? page)
        {
            ViewBag.LogLevelSort = String.IsNullOrEmpty(sort) ? "LogLevel_desc" : string.Empty;
            ViewBag.LogDateSort = sort == "LogDate" ? "LogDate_desc" : "LogDate";
            ViewBag.EntityFormalNamePluralSort = sort == "EntityFormalNamePlural" ? "EntityFormalNamePlural_desc" : "EntityFormalNamePlural";
            ViewBag.UserNameSort = sort == "UserName" ? "UserName_desc" : "UserName";
            ViewBag.MessageSort = sort == "Message" ? "Message_desc" : "Message";
            
            ViewBag.CurrentSort = sort;
            ViewBag.CurrentSearch = search;

            IQueryable<LogEntry> logEntries = db.LogEntries.OrderByDescending(le => le.LogEntryID);

            if (!string.IsNullOrEmpty(search)) logEntries = logEntries.Where(ii => ii.LogLevel.Contains(search) || ii.EntityFormalNamePlural.Contains(search) || ii.UserName.Contains(search) || ii.Message.Contains(search));

            switch (sort)
            {
                case "LogLevel_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.LogLevel);
                    break;

                case "LogDate":
                    logEntries = logEntries.OrderBy(ii => ii.LogDate);
                    break;

                case "LogDate_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.LogDate);
                    break;

                case "EntityFormalNamePlural":
                    logEntries = logEntries.OrderBy(ii => ii.EntityFormalNamePlural);
                    break;

                case "EntityFormalNamePlural_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.EntityFormalNamePlural);
                    break;

                case "UserName":
                    logEntries = logEntries.OrderBy(ii => ii.UserName);
                    break;

                case "UserName_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.UserName);
                    break;

                case "Message":
                    logEntries = logEntries.OrderBy(ii => ii.Message);
                    break;

                case "Message_desc":
                    logEntries = logEntries.OrderByDescending(ii => ii.Message);
                    break;

                default:
                    logEntries = logEntries.OrderBy(ii => ii.LogLevel);
                    break;

            }



            int pageSize = 5;
            int pageNumber = page ?? 1;

            return View(logEntries.ToPagedList(pageNumber, pageSize));
        }

        // GET: LogEntries/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LogEntry logEntry = db.LogEntries.Find(id);
            if (logEntry == null)
            {
                return HttpNotFound();
            }
            return View(logEntry);
        }

        // GET: LogEntries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LogEntries/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LogEntryID,LogDate,Logger,LogLevel,Thread,EntityFormalNamePlural,EntityKeyValue,UserName,Message,Exception")] LogEntry logEntry)
        {
            if (ModelState.IsValid)
            {
                db.LogEntries.Add(logEntry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(logEntry);
        }

        // GET: LogEntries/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LogEntry logEntry = db.LogEntries.Find(id);
            if (logEntry == null)
            {
                return HttpNotFound();
            }
            return View(logEntry);
        }

        // POST: LogEntries/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LogEntryID,LogDate,Logger,LogLevel,Thread,EntityFormalNamePlural,EntityKeyValue,UserName,Message,Exception")] LogEntry logEntry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(logEntry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(logEntry);
        }

        // GET: LogEntries/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LogEntry logEntry = db.LogEntries.Find(id);
            if (logEntry == null)
            {
                return HttpNotFound();
            }
            return View(logEntry);
        }

        // POST: LogEntries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            LogEntry logEntry = db.LogEntries.Find(id);
            db.LogEntries.Remove(logEntry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
