﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminLTE.Models;

namespace AdminLTE.Controllers
{
    public class detailOfSignaturesByChecksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: detailOfSignaturesByChecks
        public ActionResult Index(int checkId)
        {
            ViewBag.checkId = checkId;
            var detailOfSignaturesByChecks = db.detailOfSignaturesByChecks.Include(d => d.checks).Include(d => d.signatures).Where(cs => cs.checkId == checkId);
            return PartialView("_Index", detailOfSignaturesByChecks.ToList());
        }

        // GET: detailOfSignaturesByChecks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detailOfSignaturesByCheck detailOfSignaturesByCheck = db.detailOfSignaturesByChecks.Find(id);
            if (detailOfSignaturesByCheck == null)
            {
                return HttpNotFound();
            }
            return View(detailOfSignaturesByCheck);
        }

        // GET: detailOfSignaturesByChecks/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            
            //ViewBag.checkId = new SelectList(db.checks, "id", "checkNumber");
            ViewBag.signatureId = new SelectList(db.signatures.Where(s => s.active == true), "id", "name");
            //detailOfSignaturesByCheck detailOfSignaturesByCheck = new detailOfSignaturesByCheck();
            //return View();
            return PartialView("_Create");
        }

        // POST: detailOfSignaturesByChecks/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,signatureId,checkId,uniquefield,active,fieldControl,dateCreation,dateModification")] detailOfSignaturesByCheck detailOfSignaturesByCheck)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.detailOfSignaturesByChecks.Add(detailOfSignaturesByCheck);
                    check check = db.checks.Find(detailOfSignaturesByCheck.checkId);
                    db.SaveChanges();
                    Log4NetHelper.Log(
                        String.Format("Firma de cheque con ID: {0}, para el cheque con ID: {1} Agregada!",
                            detailOfSignaturesByCheck.id, detailOfSignaturesByCheck.checkId), LogLevel.INFO,
                        "detailOfSignaturesByCheck", detailOfSignaturesByCheck.id, User.Identity.Name, null);
                    return RedirectToAction("Edit", "checks", new {uniquefield = check.uniquefield});
                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    Log4NetHelper.Log(String.Format("Error al agregar una firma a un cheque"), LogLevel.ERROR,
                        "detailOfSignaturesByCheck", 0, User.Identity.Name, ex);
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:

                                TempData["msgErrorSignature"] = String.Format("¡No se puede añadir firmas duplicadas!");
                                return new ContentResult();
                                break;
                            default:
                                TempData["msgErrorSignature"] = String.Format(e.Message);
                                return new ContentResult();
                        }
                }
            }
            else
            {
                if (detailOfSignaturesByCheck.signatureId == 0)
                {
                    TempData["msgErrorSignature"] = String.Format("Debe añadir una firma válida");
                }
                else
                {
                    TempData["msgErrorSignature"] = String.Format("No se pudo añadir la firma, contacte con el administrador");
                }
            }
            
            return new ContentResult();
        }

        // GET: detailOfSignaturesByChecks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detailOfSignaturesByCheck detailOfSignaturesByCheck = db.detailOfSignaturesByChecks.Find(id);
            if (detailOfSignaturesByCheck == null)
            {
                return HttpNotFound();
            }
            ViewBag.checkId = detailOfSignaturesByCheck.checkId;
            //ViewBag.checkId = new SelectList(db.checks, "id", "checkNumber", detailOfSignaturesByCheck.checkId);
            ViewBag.signatureId = new SelectList(db.signatures.Where(s => s.active == true), "id", "name", detailOfSignaturesByCheck.signatureId);
            return PartialView("_Edit", detailOfSignaturesByCheck);
            //return PartialView("_Edit", detailOfSignaturesByCheck);
        }

        // POST: detailOfSignaturesByChecks/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,signatureId,checkId,uniquefield,active,fieldControl,dateCreation,dateModification")] detailOfSignaturesByCheck detailOfSignaturesByCheck)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(detailOfSignaturesByCheck).State = EntityState.Modified;
                    check check = db.checks.Find(detailOfSignaturesByCheck.checkId);
                    db.SaveChanges();
                    Log4NetHelper.Log(String.Format("Firma de cheque con ID: {0}, para el cheque con ID: {1} Agregada!", detailOfSignaturesByCheck.id, detailOfSignaturesByCheck.checkId), LogLevel.INFO, "detailOfSignaturesByCheck", detailOfSignaturesByCheck.id, User.Identity.Name, null);
                    return RedirectToAction("Edit", "checks", new { uniquefield = check.uniquefield });
                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    Log4NetHelper.Log(String.Format("Error al cambiar una firma a un cheque"), LogLevel.ERROR, "detailOfSignaturesByCheck", detailOfSignaturesByCheck.id, User.Identity.Name, ex);
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:
                                TempData["msgErrorSignature"] = String.Format("¡No se puede añadir firmas duplicadas!");
                                return new ContentResult();
                                break;
                            default:
                                TempData["msgErrorSignature"] = String.Format(e.Message);
                                return new ContentResult();
                        }
                }
                
            }
            else
            {
                if (detailOfSignaturesByCheck.signatureId == 0)
                {
                    TempData["msgErrorSignature"] = String.Format("Debe añadir una firma válida");
                }
                else
                {
                    TempData["msgErrorSignature"] = String.Format("No se pudo añadir la firma, contacte con el administrador");
                }
            }
            return new ContentResult();
        }

        // GET: detailOfSignaturesByChecks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            detailOfSignaturesByCheck detailOfSignaturesByCheck = db.detailOfSignaturesByChecks.Find(id);
            if (detailOfSignaturesByCheck == null)
            {
                return HttpNotFound();
            }
            ViewBag.idCheck = detailOfSignaturesByCheck.checkId;
            //return View(detailOfSignaturesByCheck);
            return PartialView("_Delete", detailOfSignaturesByCheck);
        }

        // POST: detailOfSignaturesByChecks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            detailOfSignaturesByCheck detailOfSignaturesByCheck = db.detailOfSignaturesByChecks.Find(id);
            try
            {
                check check = db.checks.Find(detailOfSignaturesByCheck.checkId);
                db.detailOfSignaturesByChecks.Remove(detailOfSignaturesByCheck);
                db.SaveChanges();
                return RedirectToAction("Edit", "checks", new { uniquefield = check.uniquefield });
            }
            catch (Exception ex)
            {
                var e = ex.GetBaseException() as SqlException;
                Log4NetHelper.Log(String.Format("Error al eliminar una firma"), LogLevel.ERROR, "detailOfSignaturesByCheck", detailOfSignaturesByCheck.id, User.Identity.Name, ex);
                if (e != null)
                {
                    TempData["msgErrorSignature"] = String.Format(e.Message);
                    return new ContentResult();
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
