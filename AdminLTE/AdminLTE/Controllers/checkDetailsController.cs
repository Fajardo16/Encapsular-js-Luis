﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AdminLTE.Models;
using PagedList;

namespace AdminLTE.Controllers
{
    public class checkDetailsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: checkDetails
        public ActionResult Index(int checkId, string search, int? page, string sort)
        {
            check check = db.checks.FirstOrDefault(c => c.id == checkId);
            ViewBag.checkUF = check.uniquefield;
            ViewBag.CurrentSearch = search;
            ViewBag.page = page;

            ViewBag.checkId = checkId;
            var checkDetails = db.checkDetails.Include(c => c.checks).Where(cs => cs.checkId == checkId);

            if (!string.IsNullOrEmpty(search)) checkDetails = checkDetails.Where(cd => cd.name.Contains(search));

            ViewBag.descriptionSort = sort == "description" ? "description_desc" : "description";
            ViewBag.amountSort = sort == "amount" ? "amount_desc" : "amount";
            ViewBag.activeSort = sort == "active" ? "active_desc" : "active";

            ViewBag.CurrentSort = sort;

            switch (sort)
            {
                case "description_desc":
                    checkDetails = checkDetails.OrderByDescending(cd => cd.name);
                    break;

                case "amount":
                    checkDetails = checkDetails.OrderBy(cd => cd.amount);
                    break;

                case "amount_desc":
                    checkDetails = checkDetails.OrderByDescending(cd => cd.amount);
                    break;

                case "active":
                    checkDetails = checkDetails.OrderBy(cd => cd.active);
                    break;

                case "active_desc":
                    checkDetails = checkDetails.OrderByDescending(cd => cd.active);
                    break;

                default:
                    checkDetails = checkDetails.OrderBy(cd => cd.name);
                    break;

            }

            int pageSize = 3;
            int pageNumber = page ?? 1;

            //return View(sales.ToPagedList(pageNumber, pageSize));

            return PartialView("_Index", checkDetails.ToPagedList(pageNumber, pageSize));
        }

        // GET: checkDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            checkDetail checkDetail = db.checkDetails.Find(id);
            if (checkDetail == null)
            {
                return HttpNotFound();
            }

            return View(checkDetail);
        }

        // GET: checkDetails/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            //ViewBag.checkId = new SelectList(db.checks, "id", "checkNumber");
            return PartialView("_Create");
        }

        // POST: checkDetails/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,checkId,name,amount,uniquefield,active,fieldControl,dateCreation,dateModification")] checkDetail checkDetail)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.checkDetails.Add(checkDetail);
                    
                    check check = db.checks.Find(checkDetail.checkId);
                    decimal registeredAmounts = amountsOfMovements(checkDetail.checkId);
                    decimal available = check.amount - registeredAmounts;

                    //Consulta para comprobar si el cheque tiene firmas
                    var availableSignatures = (from dosbc in db.detailOfSignaturesByChecks.Where(dosbc => dosbc.checkId == checkDetail.checkId)
                                               select dosbc);

                    if (availableSignatures.Any())
                    {
                        if (checkDetail.amount <= available)
                        {
                            db.SaveChanges();
                            Log4NetHelper.Log(String.Format("Movimiento de cheque con ID: {0}, para el cheque con ID: {1} Creado!", checkDetail.id, checkDetail.checkId), LogLevel.INFO, "checkDetail", checkDetail.id, User.Identity.Name, null);
                            return RedirectToAction("Edit", "checks", new { uniquefield = check.uniquefield });
                        }
                        else
                        {
                            TempData["msgErrorMovement"] = String.Format("El monto excede al disponible");
                            //return new ContentResult();
                        }
                    }
                    else
                    {
                        TempData["msgErrorMovement"] = String.Format("Primero debe agregar una firma");
                        //return new ContentResult();
                    }
                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    Log4NetHelper.Log(String.Format("Error al crear un movimiento de cheque"), LogLevel.ERROR, "CheckDetail", 0, User.Identity.Name, ex);
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:
                                TempData["msgErrorMovement"] = String.Format("Error al guardar movimientos duplicados");
                                return new ContentResult();
                                break;
                            default:
                                TempData["msgErrorMovement"] = String.Format(e.Message);
                                return new ContentResult();
                        }
                }
            }
            else
            {
                if (checkDetail.name == null || checkDetail.amount == 0)
                {
                    TempData["msgErrorMovement"] = String.Format("Ninguno de los campos puede ser nulos");
                    //return new ContentResult();
                }
                else
                {
                    TempData["msgErrorMovement"] = String.Format("No se permite caracteres especiales, ni montos en negativo.");
                    //return new ContentResult();
                }
            }

            return new ContentResult();
        }

        // GET: checkDetails/Edit/5
        public ActionResult Edit(int? id, string search, int? page, string sort)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            checkDetail checkDetail = db.checkDetails.Find(id);
            check checkNumber = db.checks.Find(checkDetail.checkId);
            if (checkDetail == null)
            {
                return HttpNotFound();
            }

            ViewBag.checkNumber = checkNumber.checkNumber;
            ViewBag.checkId = checkDetail.checkId;
            ViewBag.isActive = checkDetail.active;
            ViewBag.currentAmount = checkDetail.amount;

            ViewBag.CurrentSearch = search;
            ViewBag.page = page;
            ViewBag.sort = sort;

            return PartialView("_Edit", checkDetail);
            //return RedirectToAction("Edit/" + checkDetail.checkId, "checks");
        }

        // POST: checkDetails/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,checkId,name,amount,uniquefield,active,fieldControl,dateCreation,dateModification")] checkDetail checkDetail)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(checkDetail).State = EntityState.Modified;

                    //Calculo para disponibilidad de montos
                    check check = db.checks.Find(checkDetail.checkId);
                    decimal registeredAmounts = amountsOfMovements(checkDetail.checkId);
                    decimal available = check.amount - registeredAmounts;

                    // si el monto no excede al disponible permite la edicion
                    if (checkDetail.amount <= available)
                    {
                        db.SaveChanges();
                        Log4NetHelper.Log(
                            String.Format("Movimiento de cheque con ID: {0}, para el cheque con ID: {1} Modificado!",
                                checkDetail.id, checkDetail.checkId), LogLevel.INFO, "checkDetail", checkDetail.id,
                            User.Identity.Name, null);
                        return RedirectToAction("Edit", "checks", new {uniquefield = check.uniquefield});
                    }
                    else
                    {
                        TempData["msgErrorMovement"] = String.Format("El monto excede al disponible");
                        return new ContentResult();
                    }

                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    Log4NetHelper.Log(String.Format("Error al editar un movimiento de cheque"), LogLevel.ERROR,
                        "CheckDetail", checkDetail.id, User.Identity.Name, ex);
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:
                                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest,e.Message);
                                //return e.Message.ToString();
                                TempData["msgErrorMovement"] =
                                    String.Format("¡Error al guardar movimientos duplicados!.");
                                //return RedirectToAction("Edit/" + checkDetail.checkId, "checks");
                                return new ContentResult();
                                //ViewBag.messageToClient = String.Format("¡Esta firma ya esta registrada en el cheque!.");
                                break;
                            default:
                                TempData["msgErrorMovement"] = String.Format(e.Message);
                                return new ContentResult();
                        }
                }


            }
            else
            {
                if (checkDetail.name == null || checkDetail.amount == 0)
                {
                    TempData["msgErrorMovement"] = String.Format("Ninguno de los campos puede ser nulos");
                    //return new ContentResult();
                }
                else
                {
                    TempData["msgErrorMovement"] = String.Format("No se permite caracteres especiales, ni montos en negativo.");
                    //return new ContentResult();
                }
            }
            return new ContentResult();
        }

        // GET: checkDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            checkDetail checkDetail = db.checkDetails.Find(id);
            check check = db.checks.Find(checkDetail.checkId);
            if (checkDetail == null)
            {
                return HttpNotFound();
            }
            //ViewBag.idCheck = checkDetail.checkId;
            ViewBag.checkUF = check.uniquefield;
            return PartialView("_Delete", checkDetail);
        }

        // POST: checkDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            checkDetail checkDetail = db.checkDetails.Find(id);
            check check = db.checks.Find(checkDetail.checkId);
            db.checkDetails.Remove(checkDetail);
            try
            {
                db.SaveChanges();
                return RedirectToAction("Edit", "checks", new { uniquefield = check.uniquefield });
            }
            catch (Exception ex)
            {
                var e = ex.GetBaseException() as SqlException;
                Log4NetHelper.Log(String.Format("Error al eliminar un movimiento de cheque"), LogLevel.ERROR, "CheckDetail", checkDetail.id, User.Identity.Name, ex);
                if (e != null)
                {
                    TempData["msgErrorMovement"] = String.Format(e.Message);
                    return new ContentResult();
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            
        }

        public decimal amountsOfMovements(int checkId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var detailAmount = (from cd in db.checkDetails.Where(cd => cd.checkId == checkId && cd.active == true)
                                select cd.amount);

            if (detailAmount.Any())
            {
                var sumsAdded = detailAmount.Sum();

                return sumsAdded;
            }

            return 0;
        }


        //public JsonResult getMovement(int id)
        //{
        //    checkDetail movement = db.checkDetails.Find(id);
        //    return Json(movement, JsonRequestBehavior.AllowGet);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
