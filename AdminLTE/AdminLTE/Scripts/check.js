﻿//
//funcion abrir parcial index movimientos y firmas
function openPartialIndex(url, divId) {
    $.ajax({
        url: url,
        type: 'GET'
    })
            .done(function (response) {
                $(divId).html(response);
                inactive();
            })
            .fail(function (error) {
                alert("error");
            });
}
//muestra una vista parcial agregar movimientos y firmas
function openPartialAdd(url,div) {
    $.ajax({
        url: url,
        type: 'GET'
    })
            .done(function (response) {
                $(div).html(response).toggle(1000);
            })
            .fail(function () {
                alert("Error");
            });
}

//abrir la vista parcial de editar de movimientos y firmas
function openPartialEdit(url, $row, $buttons) {
    $('#partialCreateMovement').hide(1000);
    $('#partialCreateSignature').hide(1000);

    $.ajax({
        url: url,
        type: 'GET'
    })
        .done(function (response) {
            $row.hide();
            $buttons.unbind('click').removeAttr('href');
            $row.html(response).toggle(1000);
        })
        .fail(function () {
            alert("Error al abrir");
        });
}

//Enviar datos
function sendData(url, data) {
    $.ajax({
        url: url,
        type: 'POST',
        data: data
    })
        .done(function () {
            location.reload();
        })
        .fail(function () {
            $('#MessageToClient').html('Error al Guardar los datos');
        });
}


//funcion para verificar si mostrar inactivos esta habilitado
function inactive() {
    var inactiveMovements = $('#inactiveMovements').attr('class');
    var inactiveSignatures = $('#inactiveSignatures').attr('class');
    var off = 'activeToggle fa fa-toggle-off fa-2x';

    //console.log($class);

    if (inactiveMovements == off) {
        $('#tableCheckDetail tbody .danger').hide();
    }
    if (inactiveSignatures == off) {
        $('#tableDetailOfSignatures tbody .danger').hide();
    }
}
/**/

///////////////////////////////////////////////////////////////////////////////////
// Movimientos de cheque
var movements = {
    openPartialAdd: function(url) {
        openPartialAdd(url, '#partialCreateMovement');
    },
    openPartialEdit: function ($this, rowId) {
        var $row = $('#tableCheckDetail tbody .showData' + rowId);
        var $buttons = $('#tableCheckDetail a');
        openPartialEdit($this.attr('href'), $row, $buttons);
    },
    saveMovement: function (checkId) {
        $('#createDetailByCheck input[name=checkId]').val(checkId);
        var url = '/checkDetails/Create/';
        var data = $("#createDetailByCheck").serialize();

        sendData(url, data);
    },
    SendDataEdit: function () {
        //Recogiendo datos modificados por el usuario
        var name = $('#tableCheckDetail td input[name=name]').val();
        var amount = $('#tableCheckDetail td input[name=amount]').val();
        var active = $('#tableCheckDetail td input[name=active]').is(':checked');

        //guardando los datos en el formulario
        $('#editCheckDetailsForm input[name=name]').val(name);
        $('#editCheckDetailsForm input[name=amount]').val(amount);
        $('#editCheckDetailsForm input[name=active]').val(active);

        //asignado url y formulario para enviar
        urlEdit = '/checkDetails/Edit';
        data = $('#editCheckDetailsForm').serialize();

        sendData(urlEdit,data);
    },
    btnSearch: function($this) {
        var $search = $('#search').val();
        var url = $this.attr('href') + $search;
        var div = '#reloadIndex';
        openPartialIndex(url, div);
    },
    txtSearch: function(event,$this) {
        if (event.keyCode == 13) {
            var $search = $this.val();
            var url = $('.btnSearch').attr('href') + $search;
            var div = '#reloadIndex';
            openPartialIndex(url, div);
        }
    },
    closeCreatePartial: function() {
        $('#partialCreateMovement').hide(1000);
    },
    inactiveMovements: function () {
        $table = $('#tableCheckDetail tbody .danger');
        $table.toggle(1000);
    },
    pagination: function($this) {
        var url = $this.attr('href');
        var div = '#reloadIndex';
        openPartialIndex(url, div);
    },
    refresh: function($this) {
        var url = $this.attr('href');
        var div = '#reloadIndex';

        openPartialIndex(url, div);
    }
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Firmas de cheque
var signatures = {    
    openPartialAdd: function(url) {
        openPartialAdd(url, '#partialCreateSignature');
    },
    openPartialEdit: function ($this, rowId) {
        var $row = $('#tableDetailOfSignatures tbody .showData' + rowId);
        var $buttons = $('#tableDetailOfSignatures a');
        openPartialEdit($this.attr('href'), $row, $buttons);
    },
    saveSignature: function(checkId) {
        $('#createSignatureByCheck input[name=checkId]').val(checkId);
        var url = '/detailOfSignaturesByChecks/Create/';
        var data = $("#createSignatureByCheck").serialize();

        sendData(url, data);
    },
    SendDataEdit: function () {
        //Recogiendo datos modificados por el usuario
        signatureId = $('#tableDetailOfSignatures td select[name=signatureId] option:selected').val();
        active = $('#tableDetailOfSignatures td input[name=active]').is(':checked');

        //guardando los datos en el formulario
        $('#editSignaturesByCheckForm input[name=signatureId]').val(signatureId);
        $('#editSignaturesByCheckForm input[name=active]').val(active);

        //asignado url y formulario para enviar
        urlEdit = '/detailOfSignaturesByChecks/Edit';
        data = $('#editSignaturesByCheckForm').serialize();

        sendData(urlEdit, data);
    },
    closeCreatePartial: function () {
        $('#partialCreateSignature').hide(1000);
    },
    inactiveSignatures: function() {
        $table = $('#tableDetailOfSignatures tbody .danger');
        $table.toggle(1000);
    },
    refresh: function ($this) {
        var url = $this.attr('href');
        var div = '#partialSignature';

        openPartialIndex(url, div);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



$(window).load(function () {

    //inactive();
    //Oculta el div que contendra la vista parcial de agregar movimiento
    $('#partialCreateMovement').hide();
    
    //Oculta el div que contendra la vista parcial de agregar Firmas
    $('#partialCreateSignature').hide();

    //boton cerrar modal
    $(document).on('click', '.closeModalAdd', function (event) {
        event.preventDefault();
        $("#modalAdd").modal('toggle');
        console.log("Boton cerrar");
    });


    //Evento abrir modal
    $(document).on('click', '.btnModalUrl', function (event) {
        event.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            type: 'GET'
        })
            .done(function(response) {
                $('.contentModal').html(response);
                $("#modalAdd").modal('toggle');
            })
            .fail(function() {
                alert("Error al abrir la modal");
            });
    });
    
    //////////////////////////////////////////////
    //Listo
    //Evento abrir vista parcial agregar (firmas y movimientos)
    $('.btnPartialView').on('click', function(event) {
        event.preventDefault();
        
        // recibe el id si es Movement o Signature
        var partialId = this.id;
        if (partialId == "Movement") {
            movements.openPartialAdd($(this).attr('href'));
        }
        else if (partialId == "Signature") {
            signatures.openPartialAdd($(this).attr('href'));
        }
    });
    ////////////////////////////////////////////////////
    
    ///////////////////////////////////////////////////
    
    //Evento abrir parcial editar (movimiento y firmas)
    //Listo
    $(document).on('click', '.btnEdit', function (event) {
        event.preventDefault();

        var $obj = $(this);
        var rowId = $obj.attr('id');
        var table = $obj.attr('table-id');
        var $this = $(this);
        
        if (table == 'movements') {
            movements.openPartialEdit($this,rowId);
        } else if (table == 'signatures') {
            signatures.openPartialEdit($this,rowId);
        }
    });

    // evento llenar y enviar el formulario edicion (de movimiento y firmas)
    //Listo
    $(document).on('click', '.btnSave', function (event) {
        event.preventDefault();
        var $obj = $(this);
        var editId = $obj.attr('type-id'); //id para saber a cual vista se le aplicará el evento
        
        if (editId == 'movements') {
            movements.SendDataEdit();
        } else if (editId == 'signatures') {
            signatures.SendDataEdit();
        }
    });
    
    //Evento boton buscar
    //Listo
    $('.btnSearch').on('click', function (event) {
        event.preventDefault();
        movements.btnSearch($(this));
    });
    
    //Evento buscar con la tecla enter
    //Listo
    $('#search').on('keypress', function (event) {
        movements.txtSearch(event,$(this));
    });

    //Evento cerrar la ventana parcial crear
    //Listo
    $(document).on('click', '.closeAddPartial', function (event) {
        event.preventDefault();
        var $concat = $(this).attr('concat');
        if ($concat == 'Movement') {
            movements.closeCreatePartial();
        } else if ($concat == 'Signature') {
            signatures.closeCreatePartial();
        }
    });
    
    ///////////////////////////////////////////////////
    //Evento Mostrar y ocultar inactivos en (movimientos y firmas)
    //Listo
    $('.activeToggle').on('click', function (event) {
        event.preventDefault();
        
        var $idView = $(this).attr('id-view');
        var $activo = $(this).attr('class');
        
        if ($idView == 'movement') {
            movements.inactiveMovements();
        } else if ($idView == 'signature') {
            signatures.inactiveSignatures();
        }
        
        if ($activo == 'activeToggle fa fa-toggle-off fa-2x') {
            $(this).removeAttr();
            $(this).attr('class', 'activeToggle fa fa-toggle-on fa-2x');
            
        } else {
            $(this).removeAttr();
            $(this).attr('class', 'activeToggle fa fa-toggle-off fa-2x');
        }
    });
    
    //////////////////////////////////////////////////////
    //Evento guardar Firmas
    //Listo
    $(document).on('click', '#btnAddSignatures', function (event) {
        event.preventDefault();
        var checkId = $('#id').val();
        signatures.saveSignature(checkId);
    });
    
    //Evento guardar Movimiento
    //Listo
    $(document).on('click', '#btnAddDetails', function (event) {
        event.preventDefault();
        var checkId = $('#id').val();
        movements.saveMovement(checkId);
    });
    
    //paginacion
    //Listo
    $(document).on('click', '.pagination li a', function(event) {
        event.preventDefault();
        movements.pagination($(this));
    });
    
    //evento refrescar la vista parcial movimiento
    //ordenacion,cerrar edicion
    //Para firmas solo el evento cerrar edicion
    //Listo.
    $(document).on('click', '.refreshPartialMov', function (event) {
        event.preventDefault();

        var idBtnClose = $(this).attr('id-btn-close');

        if (idBtnClose == 'signature') {
            signatures.refresh($(this));
        } else {
            movements.refresh($(this));
        }
    });

});