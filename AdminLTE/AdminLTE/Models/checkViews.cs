﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace AdminLTE.Models
{
    public class checkViews
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public int id { get; set; }

        [Display(Name = "Estado")]
        public string checkState { get; set; }

        [Display(Name = "Número de cheque")]
        public string checkNumber { get; set; }

        [Display(Name = "Fecha")]
        [DataType(DataType.Date)]
        public DateTime date { get; set; }

        [Display(Name = "Cliente")]
        public string client { get; set; }

        [Display(Name = "Descripción")]
        public string description { get; set; }

        [Display(Name = "Moneda")]
        public string currency { get; set; }

        [Display(Name = "Tasa de cambio")]
        public decimal exchangeRate { get; set; }

        [Display(Name = "Monto Total")]
        public decimal totalAmount { get; set; }

        [Display(Name = "Movimiento")]
        public string movement { get; set; }

        [Display(Name = "Monto del movimiento")]
        public decimal movementAmount { get; set; }


        public IQueryable<checkViews> movementByCheck(int id)
        {
            IQueryable<checkViews> movementByCheck =

                (from c in db.checks
                 join cd in db.checkDetails on c.id equals cd.checkId
                 join cs in db.checkStates on c.checkStateId equals cs.id
                 join cli in db.clients on c.clientId equals cli.id
                 join cu in db.currencys on c.currencyId equals cu.id

                 where c.active == true
                 && cd.active == true
                 && c.id == id

                 select new checkViews
                 {
                     checkState = cs.name,
                     checkNumber = c.checkNumber,
                     date = c.date,
                     client = cli.name,
                     description = c.description,
                     currency = cu.name,
                     exchangeRate = c.exchangeRate,
                     totalAmount = c.amount,
                     movement = cd.name,
                     movementAmount = cd.amount
                 });

            return movementByCheck;
        }
    }
}