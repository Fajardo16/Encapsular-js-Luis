﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AdminLTE.Models
{
    public class seedMethod
    {
        public void seed(ApplicationDbContext context)
        {
            context.banks.AddOrUpdate(b => b.name,
                new bank {acronym = "BAC", name = "BAC", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now},
                new bank { acronym = "BANPRO", name = "BANPRO", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new bank { acronym = "BDF", name = "BDF", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });
            
            context.SaveChanges();

            bank bacId = context.banks.FirstOrDefault(b => b.name == "BAC");
            bank banproId = context.banks.FirstOrDefault(b => b.name == "BANPRO");
            bank bdfId = context.banks.FirstOrDefault(b => b.name == "BDF");
            
            context.SaveChanges();
            
            context.currencys.AddOrUpdate(cu => cu.name,
                new currency { name = "Córdoba", description = "Córdoba", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new currency { name = "Dolar", description = "Dolar", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });
            
            context.SaveChanges();
            
            currency currencyCordobaId = context.currencys.FirstOrDefault(cu => cu.name == "Córdoba");
            currency currencyDolarId = context.currencys.FirstOrDefault(cu => cu.name == "Dolar");
            
            context.SaveChanges();
            
            context.signatures.AddOrUpdate(si => si.name,
                new signature { name = "Yelba Carvajal", type = "A", description = "", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new signature { name = "Gerardo José Hernández Carvajal", type = "B", description = "", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });
            
            context.SaveChanges();
            
            signature signatureYelbaId = context.signatures.FirstOrDefault(si => si.name == "Yelba Carvajal");
            signature signatureGerardoId = context.signatures.FirstOrDefault(si => si.name == "Gerardo José Hernández Carvajal");
            
            context.SaveChanges();
            
            context.typeIdentifications.AddOrUpdate(ti => ti.name,
                new typeIdentification { name = "Cédula", description = "Cédula", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new typeIdentification { name = "RUC", description = "Numero Ruc", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });
            
            context.SaveChanges();
            
            typeIdentification typeIdentificationCedula = context.typeIdentifications.FirstOrDefault(ti => ti.name == "Cédula");
            typeIdentification typeIdentificationRuc = context.typeIdentifications.FirstOrDefault(ti => ti.name == "RUC");

            context.SaveChanges();

            context.clients.AddOrUpdate(c => c.name,
                new client { name = "Juan Perez", typeIdentificationId = typeIdentificationCedula.id, identification = "001-200284-0121B", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new client { name = "COPASA" , typeIdentificationId = typeIdentificationRuc.id, identification = "J2541000125408", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now});

            context.SaveChanges();

            client clientIdJuan = context.clients.FirstOrDefault(cl => cl.name == "Juan Perez");
            //client clientIdCopasa = context.clients.FirstOrDefault(cl => cl.name == "COPASA");
            
            context.SaveChanges();
            
            context.accounts.AddOrUpdate(acc => new { acc.bankId, acc.numberAccount },
                new account { bankId = bacId.id, currencyId = currencyCordobaId.id, typeAccount = "Ahorro", numberAccount = "35412569874478", balance = 50000, uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new account { bankId = bdfId.id, currencyId = currencyDolarId.id, typeAccount = "Corriente", numberAccount = "65214522541002", balance = 25000, uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });
            
            context.SaveChanges();

            context.exchangeRates.AddOrUpdate(er => er.date,
                new exchangeRate { currencyId = currencyCordobaId.id, date = new DateTime(2014, 1, 1).Date, value = 1, uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 1).Date, value = new decimal(29.0911), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 2).Date, value = new decimal(29.095), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 3).Date, value = new decimal(29.0988), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 4).Date, value = new decimal(29.1027), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 5).Date, value = new decimal(29.1066), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 6).Date, value = new decimal(29.1105), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 7).Date, value = new decimal(29.1144), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 8).Date, value = new decimal(29.1182), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 9).Date, value = new decimal(29.1221), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 10).Date, value = new decimal(29.126), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 11).Date, value = new decimal(29.1299), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 12).Date, value = new decimal(29.1338), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 13).Date, value = new decimal(29.1376), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 14).Date, value = new decimal(29.1415), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 15).Date, value = new decimal(29.1454), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 16).Date, value = new decimal(29.1493), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 17).Date, value = new decimal(29.1532), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 18).Date, value = new decimal(29.1571), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 19).Date, value = new decimal(29.161), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 20).Date, value = new decimal(29.1649), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 21).Date, value = new decimal(29.1687), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 22).Date, value = new decimal(29.1726), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 23).Date, value = new decimal(29.1765), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 24).Date, value = new decimal(29.1804), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 25).Date, value = new decimal(29.1843), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 26).Date, value = new decimal(29.1882), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 27).Date, value = new decimal(29.1921), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 28).Date, value = new decimal(29.196), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 29).Date, value = new decimal(29.1999), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new exchangeRate { currencyId = currencyDolarId.id, date = new DateTime(2016, 11, 30).Date, value = new decimal(29.2038), uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });
            
            context.SaveChanges();
            
            //Extrae la tasa de cambio de hoy
            exchangeRate exchangeRateValue = context.exchangeRates.FirstOrDefault(er => er.date.Day == DateTime.Now.Day
                && er.date.Month == DateTime.Now.Month
                && er.date.Year == DateTime.Now.Year);
            
            context.SaveChanges();

            context.checkStates.AddOrUpdate(cs => cs.name,
                new checkState { name = "Creado", description = "Creado", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new checkState { name = "Autorizado", description = "Autorizado", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new checkState { name = "Impreso", description = "Impreso", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new checkState { name = "Rechazado", description = "Rechazado", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new checkState { name = "Anulado", description = "Anulado", uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });

            context.SaveChanges();

            checkState checkStateCreated = context.checkStates.FirstOrDefault(cs => cs.name == "Creado");
            
            context.SaveChanges();
            
            context.checks.AddOrUpdate(ck => ck.checkNumber,
                new check { checkNumber = "65654138", date = DateTime.Now, description = "Cheque de prueba", clientId = clientIdJuan.id, currencyId = currencyCordobaId.id, checkStateId = checkStateCreated.id, exchangeRate = exchangeRateValue.value, amount = 10000, uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });

            context.SaveChanges();
            
            check checkId = context.checks.FirstOrDefault(ck => ck.checkNumber == "65654138");

            context.SaveChanges();

            context.detailOfSignaturesByChecks.AddOrUpdate(dsc => new { dsc.signatureId, dsc.checkId },
                new detailOfSignaturesByCheck { signatureId = signatureYelbaId.id, checkId = checkId.id, uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now },
                new detailOfSignaturesByCheck { signatureId = signatureGerardoId.id, checkId = checkId.id, uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });

            context.SaveChanges();

            context.checkDetails.AddOrUpdate(cd => new { cd.checkId, cd.name },
                new checkDetail { checkId = checkId.id, name = "Pago", amount = 10000, uniquefield = Guid.NewGuid().ToString(), active = true, dateCreation = DateTime.Now, dateModification = DateTime.Now });

            context.SaveChanges();


            /////////////////////////////////////////////////////////////////////////////////////////////////////
            ////// Modulo de Seguridad
            //var userManager =
            //    new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            //userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
            //                            {
            //                                AllowOnlyAlphanumericUserNames = false
            //                            };
            //var roleManager =
            //    new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));

            //string name = "usuario@gmail.com";
            //string password = "contraseña";
            //string firstName = "Admin";
            //string roleName = "Admin";

            //var role = roleManager.FindByName(roleName);

            //if (role == null)
            //{
            //    role = new ApplicationRole(roleName);
            //    var roleResult = roleManager.Create(role);
            //}

            //var user = userManager.FindByName(name);

            //if (user == null)
            //{
            //    user = new ApplicationUser { UserName = name, Email = name, FirstName = firstName };
            //    var result = userManager.Create(user, password);
            //    result = userManager.SetLockoutEnabled(user.Id, false);
            //}

            //var rolesForUser = userManager.GetRoles(user.Id);

            //if (!rolesForUser.Contains(role.Name))
            //{
            //    var result = userManager.AddToRole(user.Id, role.Name);
            //}
        }
    }
}