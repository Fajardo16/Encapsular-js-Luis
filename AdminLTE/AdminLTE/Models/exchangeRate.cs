﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminLTE.Models
{
    [Table("exchangerate", Schema = "adm")]
    public class exchangeRate
    {
        public exchangeRate()
        {
            uniquefield = Guid.NewGuid().ToString("X");
            active = true;
        }

        [Key]
        public int id { get; set; }

        public int currencyId { get; set; }

        [Index("INDEX_ADM_EXCHANGERATE_DATE", IsUnique = true)]
        [Required]
        public DateTime date { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.####}", ApplyFormatInEditMode = true)]
        [Required]
        public decimal value { get; set; }





        //Campos de control
        [Index("INDEX_ADM_EXCHANGERATE_UNIQUEFIELD", IsUnique = true)]
        [ScaffoldColumn(false)]
        [Display(Name = "Campo unico")]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Required]
        public string uniquefield { get; set; }

        [Display(Name = "Activo")]
        [Required]
        public bool active { get; set; }

        [ScaffoldColumn(false)]
        [Timestamp]
        public byte[] fieldControl { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateCreation { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateModification { get; set; }





        //Relaciones
        public virtual currency currencys { get; set; }
    }
}