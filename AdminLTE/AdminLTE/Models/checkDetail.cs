﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminLTE.Models
{
    [Table("checkdetail", Schema = "adm")]
    public class checkDetail
    {
        public checkDetail()
        {
            uniquefield = Guid.NewGuid().ToString();
            active = true;
        }

        [Key]
        public int id { get; set; }

        [Index("INDEX_ADM_CHECKDETAIL", 1, IsUnique = true)]
        public int checkId { get; set; }

        [Index("INDEX_ADM_CHECKDETAIL", 2, IsUnique = true)]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [RegularExpression(@"^[a-zA-Z0-9 '' ']+$", ErrorMessage = "No se permiten caracteres especiales")]
        [Display(Name = "Descripción")]
        [Required]
        public string name { get; set; }

        [RegularExpression(@"^[0-9\.]+$", ErrorMessage = "Solo se permite numeros")]
        [Display(Name = "Monto")]
        [Required]
        public decimal amount { get; set; }





        //Campos de control
        [Index("INDEX_ADM_CHECKDETAIL_UNIQUEFIELD", IsUnique = true)]
        [ScaffoldColumn(false)]
        [Display(Name = "Campo unico")]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Required]
        public string uniquefield { get; set; }

        [Display(Name = "Activo")]
        [Required]
        public bool active { get; set; }

        [ScaffoldColumn(false)]
        [Timestamp]
        public byte[] fieldControl { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateCreation { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateModification { get; set; }





        //Relaciones
        public virtual check checks { get; set; }
    }
}