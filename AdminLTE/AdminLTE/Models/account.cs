﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminLTE.Models
{
    [Table("account", Schema = "adm")]
    public class account
    {
        public account()
        {
            uniquefield = Guid.NewGuid().ToString("X");
            active = true;
        }

        [Key]
        public int id { get; set; }

        [Index("INDEX_ADM_ACCOUNT", 1, IsUnique = true)]
        public int bankId { get; set; }

        public int currencyId { get; set; }

        [Display(Name = "Tipo de cuenta")]
        public string typeAccount { get; set; }

        [Index("INDEX_ADM_ACCOUNT", 2, IsUnique = true)]
        [MaxLength(20, ErrorMessage = "Máximo {0} caracteres")]
        [Display(Name = "Número de cuenta")]
        [Required]
        public string numberAccount { get; set; }

        [Display(Name = "Saldo")]
        public decimal balance { get; set; }





        //Campos de control
        [Index("INDEX_ADM_ACCOUNT_UNIQUEFIELD", IsUnique = true)]
        [ScaffoldColumn(false)]
        [Display(Name = "Campo unico")]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Required]
        public string uniquefield { get; set; }

        [Display(Name = "Activo")]
        [Required]
        public bool active { get; set; }

        [ScaffoldColumn(false)]
        [Timestamp]
        public byte[] fieldControl { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateCreation { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateModification { get; set; }





        //Relaciones
        public virtual bank banks { get; set; }
        public virtual currency currencys { get; set; }
    }
}