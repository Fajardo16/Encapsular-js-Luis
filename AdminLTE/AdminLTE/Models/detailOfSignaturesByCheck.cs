﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminLTE.Models
{
    [Table("detailofsignaturesbycheck", Schema = "adm")]
    public class detailOfSignaturesByCheck
    {
        public detailOfSignaturesByCheck()
        {
            uniquefield = Guid.NewGuid().ToString();
            active = true;
        }

        [Key]
        public int id { get; set; }

        [Index("INDEX_ADM_DETAILOFSIGNATURESBYCHECK", 1, IsUnique = true)]
        public int signatureId { get; set; }

        [Index("INDEX_ADM_DETAILOFSIGNATURESBYCHECK", 2, IsUnique = true)]
        public int checkId { get; set; }





        //Campos de control
        [Index("INDEX_ADM_DETAILOFSIGNATURESBYCHECK_UNIQUEFIELD", IsUnique = true)]
        [ScaffoldColumn(false)]
        [Display(Name = "Campo unico")]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Required]
        public string uniquefield { get; set; }

        [Display(Name = "Activo")]
        [Required]
        public bool active { get; set; }

        [ScaffoldColumn(false)]
        [Timestamp]
        public byte[] fieldControl { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateCreation { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateModification { get; set; }





        //Relaciones
        public virtual signature signatures { get; set; }
        public virtual check checks { get; set; }
    }
}