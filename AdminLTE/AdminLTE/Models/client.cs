﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminLTE.Models
{
    [Table("client", Schema = "adm")]
    public class client
    {
        public client()
        {
            uniquefield = Guid.NewGuid().ToString("X");
            active = true;
        }

        [Key]
        public int id { get; set; }

        [Index("INDEX_ADM_CLIENT_NAME", IsUnique = true)]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Display(Name = "Nombre")]
        [Required]
        public string name { get; set; }

        [Display(Name = "Tipo de identificación")]
        public int typeIdentificationId { get; set; }

        [Index("INDEX_ADM_CLIENT_IDENTIFICATION", IsUnique = true)]
        [MaxLength(20, ErrorMessage = "Máximo {0} caracteres")]
        public string identification { get; set; }





        //Campos de control
        [Index("INDEX_ADM_CLIENT_UNIQUEFIELD", IsUnique = true)]
        [ScaffoldColumn(false)]
        [Display(Name = "Campo unico")]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Required]
        public string uniquefield { get; set; }

        [Display(Name = "Activo")]
        [Required]
        public bool active { get; set; }

        [ScaffoldColumn(false)]
        [Timestamp]
        public byte[] fieldControl { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateCreation { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateModification { get; set; }





        //Relaciones
        public virtual typeIdentification typeIdentifications { get; set; }
    }
}