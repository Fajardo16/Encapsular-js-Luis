﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AdminLTE.Models
{
    // Puede agregar datos del perfil del usuario agregando más propiedades a la clase ApplicationUser. Para más información, visite http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar reclamaciones de usuario personalizado aquí
            return userIdentity;
        }

        public string FirstName { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<account> accounts { get; set; }
        public DbSet<bank> banks { get; set; }
        public DbSet<check> checks { get; set; }
        public DbSet<checkDetail> checkDetails { get; set; }
        public DbSet<checkState> checkStates { get; set; }
        public DbSet<client> clients { get; set; }
        public DbSet<currency> currencys { get; set; }
        public DbSet<detailOfSignaturesByAccount> detailsOfSignaturesByAccounts { get; set; }
        public DbSet<detailOfSignaturesByCheck> detailOfSignaturesByChecks { get; set; }
        public DbSet<exchangeRate> exchangeRates { get; set; }
        public DbSet<signature> signatures { get; set; }
        public DbSet<typeIdentification> typeIdentifications { get; set; }

        public DbSet<LogEntry> LogEntries { get; set; }
    }
}