﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Compilation;

namespace AdminLTE.Models
{
    [Table("check", Schema = "adm")]
    public class check
    {
        public check()
        {
            uniquefield = Guid.NewGuid().ToString();
            active = true;
        }

        [Key]
        public int id { get; set; }

        [Index("INDEX_ADM_CHECK_CHECKNUMBER", IsUnique = true)]
        [MaxLength(20, ErrorMessage = "Máximo {0} caracteres")]
        [Display(Name = "Numero de cheque")]
        [Required]
        public string checkNumber { get; set; }

        [Display(Name = "Fecha")]
        [Required]
        public DateTime date { get; set; }

        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Display(Name = "Descripción")]
        public string description { get; set; }

        [Required]
        public int clientId { get; set; }

        [Required]
        public int currencyId { get; set; }

        [Required]
        public int checkStateId { get; set; }

        [Required]
        public decimal exchangeRate { get; set; }

        [RegularExpression(@"^[0-9\.]+$", ErrorMessage = "Solo se permite números")]
        [Display(Name = "Monto Total")]
        [Required]
        public decimal amount { get; set; }





        //Campos de control
        [Index("INDEX_ADM_CHECK_UNIQUEFIELD", IsUnique = true)]
        [ScaffoldColumn(false)]
        [Display(Name = "Campo unico")]
        [MaxLength(128, ErrorMessage = "Máximo {0} caracteres")]
        [Required]
        public string uniquefield { get; set; }

        [Display(Name = "Activo")]
        [Required]
        public bool active { get; set; }

        [ScaffoldColumn(false)]
        [Timestamp]
        public byte[] fieldControl { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateCreation { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime dateModification { get; set; }





        //Relaciones
        public virtual client clients { get; set; }
        public virtual currency currencys { get; set; }

        public virtual checkState checkStates { get; set; }
        //public virtual exchangeRate exchangeRates { get; set; }
    }
}