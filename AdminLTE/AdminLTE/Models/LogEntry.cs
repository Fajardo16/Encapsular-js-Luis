﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminLTE.Models
{
    public class LogEntry
    {
        public Int64 LogEntryID { get; set; }

        [Required]
        [Display(Name = "Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LogDate { get; set; }

        [Required]
        [MaxLength(30, ErrorMessage = "Máximo {0} caracteres")]
        public string Logger { get; set; }

        [Required]
        [Display(Name = "Level")]
        [MaxLength(5, ErrorMessage = "Máximo {0} caracteres")]
        public string LogLevel { get; set; }

        [Required]
        [MaxLength(10, ErrorMessage = "Máximo {0} caracteres")]
        public string Thread { get; set; }

        [Display(Name = "Entity")]
        [Required]
        [MaxLength(50, ErrorMessage = "Máximo {0} caracteres")]
        [Index("IDX_LogEntries_Entity", Order = 2)]
        public string EntityFormalNamePlural { get; set; }

        [Display(Name = "Key")]
        [Required]
        [Index("IDX_LogEntries_Entity", Order = 1)]
        public int EntityKeyValue { get; set; }

        [Display(Name = "Usuario")]
        [MaxLength(256, ErrorMessage = "Máximo {0} caracteres")]
        public string UserName { get; set; }

        [MaxLength(256, ErrorMessage = "Máximo {0} caracteres")]
        public string Message { get; set; }

        public string Exception { get; set; }
    }
}